import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ChainFinderTest {

    ChainFinder finder;
    ChainFinder finder2;
    Graph graph;

    @Before
    public void init() {
        graph = new Graph();
        finder = new ChainFinder(3, "src/main/resources/testDictionary.txt", graph);
        finder2 = new ChainFinder(4, "src/main/resources/dictionary1.txt");
    }

    @Test
    public void findSimilarWords() throws Exception {

        finder.findWordChain("AAA", "ABB");
        assertEquals(graph.getVertexes().size(), 12);
        assertEquals(graph.getVertexes().get(0).getNeighbours().size(), 6);
        assertEquals(graph.getVertexes().get(1).getNeighbours().size(), 5);
        assertEquals(graph.getVertexes().get(2).getNeighbours().size(), 4);

    }

    @Test
    public void findChain() throws Exception {
        List<String> correctWordChain = finder.findWordChain("AAA", "AAB");
        List<String> sameInputWordChain = finder.findWordChain("AAA", "AAA");

        assertEquals(correctWordChain.size(), 2);
        assertEquals(sameInputWordChain.size(), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findChainShouldThrowExceptionWhenFirstArgNoExistInDictionary() throws Exception {
        List<String> illegalArgsInputWordChain = finder.findWordChain("762", "AAA");
    }

    @Test(expected = IllegalArgumentException.class)
    public void findChainShouldThrowExceptionWhenSecondArgNoExistInDictionary() throws Exception {
        List<String> illegalArgsInputWordChain = finder.findWordChain("AAA", "545");
    }

    @Test(expected = IllegalArgumentException.class)
    public void findChainShouldThrowExceptionWhenArgsLengthIsNotEqual() throws Exception {
        List<String> illegalArgsInputWordChain = finder.findWordChain("AAA", "545");
    }

    @Test
    public void isSimilarTest() throws Exception {

        List<String> wordChain = finder2.findWordChain("Atco", "froe");
        for (int i = 0; i < wordChain.size() - 1; i++) {
            assertTrue(finder2.isSimilar(wordChain.get(i), wordChain.get(i + 1), 1));
        }
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenNoFileGiven() throws Exception {
        ChainFinder chFinder = new ChainFinder(3, null);
        chFinder.findWordChain("AAA", "ABB");
    }
}