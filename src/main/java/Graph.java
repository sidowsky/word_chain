import java.util.ArrayList;
import java.util.List;

/**
 * Created by sid on 2018-04-10.
 */
public class Graph {

    private List<Vertex> vertexes;

    public Graph() {
        this.vertexes = new ArrayList<Vertex>();
    }

    public List<Vertex> getVertexes() {
        return vertexes;
    }

    public void setVertexes(List<Vertex> vertexes) {
        this.vertexes = vertexes;
    }

    public void addNewVertex(Vertex newVertex) {
        this.vertexes.add(newVertex);
    }


}
