import java.util.ArrayList;

/**
 * Created by sid on 2018-04-10.
 */
public class Vertex {

    /**
     * Value stored by vertex
     */
    private String value;
    /**
     * Collections contains all neighbours
     *
     * @see ChainFinder#findSimilarWords(String)
     */
    private ArrayList<Vertex> neighbours;

    /**
     * Field used by traversal algorithm
     */
    private Visited isVisited;
    /**
     * Field used by traversal algorithm
     */
    private Vertex parent;

    /**
     * Field used by traversal algorithm
     */
    private int distanceFromOrigin;

    public Vertex() {
    }

    public Vertex(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ArrayList<Vertex> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(ArrayList<Vertex> neighbours) {
        this.neighbours = neighbours;
    }

    public Visited getIsVisited() {
        return isVisited;
    }

    public void setIsVisited(Visited isVisited) {
        this.isVisited = isVisited;
    }

    public Vertex getParent() {
        return parent;
    }

    public void setParent(Vertex parent) {
        this.parent = parent;
    }

    public int getDistanceFromOrigin() {
        return distanceFromOrigin;
    }

    public void setDistanceFromOrigin(int distanceFromOrigin) {
        this.distanceFromOrigin = distanceFromOrigin;
    }

    /**
     * Enum used by traversal algorithm - informs about vertex availability.
     * WHITE - vertex never visited, able to visit
     * GREY - vertex visited, able to retrieve data from vertex
     * BLACK - vertex visited, unable to work with this vertex anymore
     */
    public enum Visited {
        WHITE, GREY, BLACK
    }
}
