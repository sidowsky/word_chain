import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ChainFinder {

    private static final int MAX_MISTAKES = 1;
    private ArrayList<String> dictionary;
    private BFS bfs;
    private Graph graph;

    private String path;
    private int wordLength;
    private boolean isInit;

    public ChainFinder() {

    }

    /**
     * Create subdictionary based on input file, contains words about length as same as given 'start' and 'end' word
     *
     * @param wordLength Length of 'begin' and 'end' words - it's not necessary to create graph based on all words
     *                    from dictionary, only wods of that length can be used to create word chain
     * @param pathToFile  Path to file with words
     */
    public ChainFinder(int wordLength, String pathToFile) {

        this.path = pathToFile;
        this.wordLength = wordLength;
        this.bfs = new BFS();
        this.dictionary = new ArrayList<>();
        this.graph = new Graph();
        this.isInit = false;
    }

    /**
     * Constructor for test purposes
     *
     * @param wordLength Length of 'begin' and 'end' words - it's not necessary to create graph based on all words
     *                    from dictionary, only wods of that length can be used to create word chain
     * @param pathToFile  Path to file with words
     * @param graph      Graph
     */
    public ChainFinder(int wordLength, String pathToFile, Graph graph) {
        this(wordLength, pathToFile);
        this.graph = graph;
    }

    /**
     * This method find shortest path from 'begin' to 'end'. It use BFS graph traversal algorithm to find shorthest path.
     *
     * @param begin first word in word chain
     * @param end   second word in word chain
     * @return shortest word chain from 'begin' to 'end'
     */
    public List<String> findWordChain(String begin, String end) {

        if (!isInit) {
            readDataFromFile(wordLength, path);
            initGraph();
            initEdges();
            isInit = true;
        }

        if (begin.length() != end.length()) {
            throw new IllegalArgumentException("Words have different length");
        }
        if (begin.equals(end)) {
            return Collections.emptyList();
        }
        if (!dictionary.contains(begin)) {
            throw new IllegalArgumentException("'Begin' word doesn't exist in dictionary");
        }
        if (!dictionary.contains(end)) {
            throw new IllegalArgumentException("'End' word doesn't exist in dictionary");
        }

        bfs.findPaths(graph, findVertexByName(begin));

        List<Vertex> path = new LinkedList<>();
        Vertex endVertex = findVertexByName(end);
        path.add(endVertex);
        while (endVertex.getParent() != null) {
            path.add(endVertex.getParent());
            endVertex = endVertex.getParent();
        }

        if (!path.get(path.size() - 1).getValue().equals(begin)) {
            throw new IllegalArgumentException("Path between 'begin' and 'end' not exist");
        }

        List<String> pathAsStringList = new LinkedList<>();

        for (Vertex vertex : path) {
            pathAsStringList.add(vertex.getValue());
        }

        Collections.reverse(pathAsStringList);
        GraphUtils.restoreDefaultValues(graph);
        return pathAsStringList;
    }

    /**
     * Checks the identity of words. Check letters on the same position in both words.
     *
     * @param word            first word to compare
     * @param wordToCompare   second word to compare
     * @param mistakesAllowed maximum number of different letters allowed in words
     * @return True if words are the same or number of differences are less than given value. In other cases returns false
     */
    public boolean isSimilar(String word, String wordToCompare, int mistakesAllowed) {
        if (word.equals(wordToCompare))
            return true;
        if (word.length() == wordToCompare.length()) {
            for (int i = 0; i < word.length(); i++) {
                if (word.charAt(i) != wordToCompare.charAt(i)) {
                    mistakesAllowed--;
                    if (mistakesAllowed < 0) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    /**
     * Read all words of given length from file
     *
     * @param wordLength length all words to import
     * @param pathToFile path to dictionary
     */
    private void readDataFromFile(int wordLength, String pathToFile) {
        String line;
        try {
            FileReader fileReader = new FileReader(pathToFile);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while ((line = bufferedReader.readLine()) != null) {
                if (line.length() == wordLength) {
                    dictionary.add(line);
                }
            }
            bufferedReader.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Init graph by creating vertexes based on ChainFinder.dictionary
     */
    private void initGraph() {
        for (String word : dictionary) {
            graph.addNewVertex(new Vertex(word));
        }
    }

    /**
     * Create edge between two edges (w,v), only w is equal to v except one letter
     */
    private void initEdges() {
        for (Vertex vertex : graph.getVertexes()) {
            String value = vertex.getValue();
            vertex.setNeighbours(findSimilarWords(value));
        }
    }

    /**
     * Find all similar words
     *
     * @param value
     * @return all similar words to given value
     * @see ChainFinder#isSimilar
     */
    private ArrayList<Vertex> findSimilarWords(String value) {

        ArrayList<Vertex> similarWords = new ArrayList<>();

        for (String word : dictionary) {
            if (!word.equals(value)) {
                if (isSimilar(value, word, MAX_MISTAKES)) {
                    similarWords.add(findVertexByName(word));
                }
            }
        }

        return similarWords;
    }

    /**
     * Find vertex representing given String
     *
     * @param word value
     * @return Vertex representing given value
     */
    private Vertex findVertexByName(String word) {

        for (Vertex vertex : graph.getVertexes()) {
            if (vertex.getValue().equals(word))
                return vertex;
        }
        throw new RuntimeException();
    }


}
