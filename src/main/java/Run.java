import java.util.List;

/**
 * Created by sid on 2018-04-10.
 */
public class Run {

    private static final String PATH_TO_MAIN_DICTIONARY = "src/main/resources/dictionary1.txt";
    private static final String PATH_DO_TEST_DICTIONARY = "src/main/resources/testDictionary.txt";
    private static final int WORD_SIZE_THREE = 3;
    private static final int WORD_SIZE_FOUR = 4;


    public static void main(String[] args) {

        ChainFinder chainFinder = new ChainFinder(WORD_SIZE_THREE, PATH_TO_MAIN_DICTIONARY);
        ChainFinder chainFinder1 = new ChainFinder(WORD_SIZE_THREE, PATH_DO_TEST_DICTIONARY);
        ChainFinder chainFinder2 = new ChainFinder(WORD_SIZE_FOUR, PATH_TO_MAIN_DICTIONARY);

        List<String> wordChain = chainFinder1.findWordChain("AAA", "AAB");
        List<String> wordChain1 = chainFinder.findWordChain("cat", "dog");
        List<String> wordChain2 = chainFinder.findWordChain("cat", "cat");
        List<String> wordChain3 = chainFinder2.findWordChain("Atco", "froe");
        List<String> wordChain4 = chainFinder.findWordChain("cat", "bat");

        GraphUtils.printPath(wordChain);
        GraphUtils.printPath(wordChain1);
        GraphUtils.printPath(wordChain2);
        GraphUtils.printPath(wordChain3);
        GraphUtils.printPath(wordChain4);

    }
}
