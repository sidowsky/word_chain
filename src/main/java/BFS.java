import java.util.LinkedList;

/**
 * This class implements BFS shorthest path algorithm, to find shortest path from source, to all others vertexes.
 */
public class BFS {


    /**
     * Shorthest path in graph
     *
     * @param graph  Graph represents words
     * @param origin Vertex represents 'begin' word in chain
     */
    public void findPaths(Graph graph, Vertex origin) {

        LinkedList<Vertex> fifoQueue = new LinkedList<>();

        Vertex currentVertex;

        for (Vertex vertex : graph.getVertexes()) {
            vertex.setIsVisited(Vertex.Visited.WHITE);
            vertex.setParent(null);
            vertex.setDistanceFromOrigin(0);
        }
        origin.setIsVisited(Vertex.Visited.GREY);
        fifoQueue.add(origin);
        while (!fifoQueue.isEmpty()) {

            currentVertex = fifoQueue.removeFirst();

            for (Vertex neighbour : currentVertex.getNeighbours()) {
                if (neighbour.getIsVisited() == Vertex.Visited.WHITE) {
                    neighbour.setIsVisited(Vertex.Visited.GREY);
                    neighbour.setDistanceFromOrigin(currentVertex.getDistanceFromOrigin() + 1);
                    neighbour.setParent(currentVertex);
                    fifoQueue.add(neighbour);
                }
            }
            currentVertex.setIsVisited(Vertex.Visited.BLACK);
        }
    }
}
