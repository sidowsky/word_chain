import java.util.List;

/**
 * Created by sid on 2018-04-22.
 */
public class GraphUtils {

    /**
     * Print path and show how many transforms are needed to transform begin to end.
     * In fact, this is number of edges on shortest path
     *
     * @param wordChain
     */
    public static void printPath(List<String> wordChain) {

        if (wordChain.size() == 0) {
            System.out.println("Begin and end word are the same");
            return;
        }

        System.out.println("Number transforms needed to transform " +
                wordChain.get(0) +
                " to " +
                wordChain.get(wordChain.size() - 1) +
                " : " +
                (wordChain.size() - 1));
        for (String word :
                wordChain) {
            System.out.println(word);
        }
    }

    /**
     * Set all vertex as unvisited, to reuse ChainFinder object
     */
    public static void restoreDefaultValues(Graph graph) {
        for (Vertex vertex : graph.getVertexes()) {
            vertex.setIsVisited(Vertex.Visited.WHITE);
            vertex.setParent(null);
            vertex.setDistanceFromOrigin(0);
        }
    }
}
